<?php

/**
 * @file
 * User name from ID.
 */
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function uname_uid_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.uname_uid':
      return 'User name from ID.';
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function uname_uid_entity_extra_field_info() {
  $fields = array();
  $fields['user']['user']['form'] = array();
  $form = &$fields['user']['user']['form'];

  $form['customize_name'] = array(
    'label' => t('Customize user name'),
    'weight' => -8
  );

  return $fields;
}

/**
 * API
 */

/**
 * Check if user name customized.
 * 
 * @param UserInterface $user
 * @return boolean
 */
function uname_uid_is_username_customized(UserInterface $user) {
  if (!$user->id()) {
    return false;
  }
  /* @var $data \Drupal\user\UserData */
  $data = \Drupal::service('user.data');
  if ($data->get('uname_uid', $user->id(), 'customized') == '1') {
    return true;
  }
  return false;
}

/**
 * Module
 */

/**
 * Implements hook_ENTITY_TYPE_presave() for user entities.
 */
function uname_uid_user_presave(UserInterface $user) {
  if (isset($user->customizeUsername) && $user->customizeUsername == 0) {
    if ($user->isNew()) {
      $user->set('uid', $user->getUsername());
    }
    $user->setUsername($user->id());
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert() for user entities.
 */
function uname_uid_user_insert(UserInterface $user) {
  uname_uid_user_save($user);
}

/**
 * Implements hook_ENTITY_TYPE_update() for user entities.
 */
function uname_uid_user_update(UserInterface $user) {
  uname_uid_user_save($user);
}

/**
 * Save is customized to oser data.
 * 
 * @param UserInterface $user
 */
function uname_uid_user_save(UserInterface $user) {
  $customized = 0;
  if (isset($user->customizeUsername) && $user->customizeUsername === 1) {
    $customized = 1;
  }
  /* @var $data \Drupal\user\UserData */
  $data = \Drupal::service('user.data');
  $data->set('uname_uid', $user->id(), 'customized', $customized);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function uname_uid_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ((array_key_exists('account', $form) && array_key_exists('name', $form['account'])) || array_key_exists('name', $form)) {
    $account = &$form['account'];
    if (!array_key_exists('name', $account)) {
      $account = &$form;
    }
    $name = &$account['name'];
    if (\Drupal::currentUser()->hasPermission('customize user name')) {
      $account['customize_name'] = array(
        '#type' => 'checkbox',
        '#title' => t('Customize user name'),
        '#access' => $name['#access'],
        '#default_value' => uname_uid_is_username_customized($form_state->getFormObject()->getEntity())
      );
      $name['#required'] = false;
      $name['#element_validate'][] = 'uname_uid_user_name_validate';
      $name['#states']['visible'][':input[name="customize_name"]'] = array('checked' => true);
      $name['#states']['required'][':input[name="customize_name"]'] = array('checked' => true);
      $form['#entity_builders'][] = 'uname_uid_user_builder_customize';
    }
    else {
      $form['#entity_builders'][] = 'uname_uid_user_builder';
      $name['#access'] = false;
    }
  }
}

/**
 * Validate user name.
 */
function uname_uid_user_name_validate(array $element, FormStateInterface $form_state) {
  if ($form_state->getValue('customize_name', 0)) {
    if (strlen($form_state->getValue('name')) == 0) {
      $form_state->setError($element, t('!name field is required.', array('!name' => $element['#title'])));
    }
    else if (is_numeric($form_state->getValue('name'))) {
      $form_state->setError($element, t('!name should be letters or numbers and letters.', array('!name' => $element['#title'])));
    }
  }
}

/**
 * Entity form builder to add the username to the user.
 */
function uname_uid_user_builder_customize($entity_type, UserInterface $user, &$form, FormStateInterface $form_state) {
  $user->customizeUsername = $form_state->getValue('customize_name', 0);
  if (!$form_state->getValue('customize_name', 0)) {
    uname_uid_user_builder($entity_type, $user, $form, $form_state);
  }
}

/**
 * Entity form builder to add the username to the user.
 */
function uname_uid_user_builder($entity_type, UserInterface $user, &$form, FormStateInterface $form_state) {
  if (!$user->isNew()) {
    $user->setUsername($user->id());
  }
  else {
    $entity_manager = \Drupal::entityManager();
    $last_uid = current($entity_manager->getStorage($entity_type)->getQuery()->sort('uid', 'DESC')->pager(1)->execute());
    $last_uid++;
    $user->setUsername($last_uid);
  }
  $form_state->setValue('name', $user->getUsername());
}
